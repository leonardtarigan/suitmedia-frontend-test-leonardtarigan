export type IdeasImage = {
  id: number;
  file_name: string;
  mime: string;
  url: string;
};

export type Ideas = {
  id: number;
  slug: string;
  content: string;
  title: string;
  created_at: string;
  published_at: string;
  updated_at: string;
  deleted_at: string | null;
  small_image: IdeasImage[];
  medium_image: IdeasImage[];
};

export type ResponseMeta = {
  current_page: number;
  from: number;
  last_page: number;
  per_page: number;
  to: number;
  total: number;
};

export type IdeasResponse = {
  data: Ideas[];
  meta: ResponseMeta;
};

export type GetIdeasProps = {
  page: number;
  pageSize: number;
  sort: string;
  append: string[];
};
