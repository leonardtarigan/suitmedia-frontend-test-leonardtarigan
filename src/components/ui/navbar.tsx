"use client";

import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { HamburgerMenuIcon } from "../icons/hamburger-menu-icon";
import { useEffect, useState } from "react";
import { XMarkIcon } from "../icons/x-mark-icon";

const menus = ["work", "about", "services", "ideas", "careers", "contact"];

function Navbar() {
  const params = usePathname();

  const [showMainNav, setShowMainNav] = useState(true);
  const [showSecondaryNav, setShowSecondaryNav] = useState(false);
  const [showMobileNav, setShowMobileNav] = useState(false);

  useEffect(() => {
    let prevScrollY = 0;

    const handleScroll = () => {
      const currentScrollY = window.scrollY;

      if (currentScrollY > 10) {
        setShowMainNav(false);

        if (currentScrollY > prevScrollY) {
          setShowSecondaryNav(false);
        } else if (currentScrollY < prevScrollY) {
          setShowSecondaryNav(true);
        }
      } else {
        setShowSecondaryNav(false);
        setShowMainNav(true);
      }

      prevScrollY = currentScrollY;
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [showMainNav]);

  return (
    <nav
      className={`fixed w-full z-40 transition-all duration-300 flex justify-between py-5 px-5 sm:px-10 md:px-20 bg-suitmedia-orange text-white ${
        showMainNav ? "top-0" : "-top-full"
      } ${
        showSecondaryNav ? "top-0 bg-opacity-70 backdrop-blur-sm" : "-top-0 "
      }`}
    >
      <Link href={"/"} className="relative aspect-[16/6] z-10 h-10">
        <Image src={"/logo-white.png"} alt="Logo" fill />
      </Link>

      <div className="hidden md:flex gap-7 capitalize items-center">
        {menus.map((menu, index) => (
          <Link
            href={`/${menu}`}
            key={index}
            className={`border-b-4 py-2 ${
              params.includes(menu) ? "border-white" : "border-transparent"
            }`}
          >
            {menu}
          </Link>
        ))}
      </div>

      <button onClick={() => setShowMobileNav(true)} className="md:hidden">
        <HamburgerMenuIcon className="w-10 h-10" />
      </button>

      <div
        className={`w-2/3 h-screen flex flex-col gap-10 items-end z-50 fixed transition-all duration-300 p-5 bg-suitmedia-orange top-0 shadow-lg ${
          showMobileNav ? "right-0" : "-right-full"
        }`}
      >
        <button onClick={() => setShowMobileNav(false)}>
          <XMarkIcon className="h-10 w-10" />
        </button>
        <div className="flex flex-col capitalize text-end">
          {menus.map((menu, index) => (
            <Link
              href={`/${menu}`}
              onClick={() => setShowMobileNav(false)}
              key={index}
              className={`border-b-4 py-2 ${
                params.includes(menu) ? "border-white" : "border-transparent"
              }`}
            >
              {menu}
            </Link>
          ))}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
