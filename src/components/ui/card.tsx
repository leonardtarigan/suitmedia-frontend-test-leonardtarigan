import { formatDate } from "@/lib/utils";
import Image from "next/image";

type CardProps = {
  published_at: string;
  title: string;
  image_url: string;
};

function Card({ published_at, title, image_url }: CardProps) {
  return (
    <div className="shadow-lg overflow-hidden rounded-lg">
      <div className="w-full relative bg-gray-400 aspect-[16/10]">
        <Image src={image_url} alt={title} fill className="object-cover" />
      </div>
      <div className="p-5">
        <p className="font-semibold text-xs uppercase text-gray-400">
          {formatDate(published_at)}
        </p>
        <h2 className="line-clamp-3 font-semibold">{title}</h2>
      </div>
    </div>
  );
}

export default Card;
