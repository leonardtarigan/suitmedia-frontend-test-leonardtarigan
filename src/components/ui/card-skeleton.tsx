function CardSkeleton({ size }: { size: number }) {
  return (
    <>
      {[...Array(size)].map((_, index) => {
        return (
          <div
            key={index}
            className="shadow-lg bg-gray-300 animate-pulse aspect-[8/9] overflow-hidden rounded-lg"
          ></div>
        );
      })}
    </>
  );
}

export default CardSkeleton;
