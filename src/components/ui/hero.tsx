import Image from "next/image";
import { Background, Parallax } from "react-parallax";

function HeroSection() {
  return (
    <Parallax
      strength={300}
      className="h-[35rem] w-full text-center clip-polygon z-0 relative text-white flex flex-col justify-center items-center"
    >
      <Background className="w-screen h-[40rem] border-2 relative">
        <Image
          src={
            "https://images.unsplash.com/photo-1598520106830-8c45c2035460?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
          }
          alt="Hero Image"
          fill
          className="object-cover -z-10 brightness-50"
        />
      </Background>
      <h1 className="font-medium text-5xl">Ideas</h1>
      <p>Where all our great things begin</p>
    </Parallax>
  );
}

export default HeroSection;
