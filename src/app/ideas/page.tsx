"use client";

import { ChevronDoubleLeftIcon } from "@/components/icons/chevron-double-left-icon";
import { ChevronDoubleRightIcon } from "@/components/icons/chevron-double-right-icon";
import { ChevronLeftIcon } from "@/components/icons/chevron-left-icon";
import { ChevronRightIcon } from "@/components/icons/chevron-right-icon";
import Card from "@/components/ui/card";
import CardSkeleton from "@/components/ui/card-skeleton";
import HeroSection from "@/components/ui/hero";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Ideas, ResponseMeta } from "@/lib/types";
import { calculatePagesToShow } from "@/lib/utils";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import GET from "../api/get";

function IdeasPage({
  searchParams,
}: {
  searchParams: { [key: string]: string | undefined };
}) {
  const router = useRouter();

  const currentPage = parseInt(searchParams["page[number]"] ?? "1");
  const currentPageSize = parseInt(searchParams["page[size]"] ?? "10");
  const currentSort = searchParams["sort"] ?? "-published_at";

  const [ideasList, setIdeasList] = useState<Ideas[]>();
  const [ideasMeta, setIdeasMeta] = useState<ResponseMeta>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);

      const res = await GET.ideas({
        page: currentPage,
        pageSize: currentPageSize,
        sort: currentSort,
        append: ["medium_image", "small_image"],
      });

      setIdeasList(res?.data);
      setIdeasMeta(res?.meta);
      setIsLoading(false);
    };

    getData();
  }, [currentPage, currentPageSize, currentSort, searchParams]);

  const totalPage = ideasMeta?.last_page;

  const paginationNavItems = calculatePagesToShow(
    currentPage,
    totalPage ?? currentPage
  );

  return (
    <main className="pb-20 max-w-screen-2xl mx-auto">
      <HeroSection />
      <section className="flex  gap-10 flex-col mt-20 px-5 sm:px-10 md:px-20">
        <div className="flex gap-2 flex-wrap items-center font-medium justify-between">
          <p>
            Showing {ideasMeta?.from} - {ideasMeta?.to} of {ideasMeta?.total}
          </p>
          <div className="flex flex-wrap gap-2 md:gap-10">
            <div className="flex gap-2 items-center">
              <span className="whitespace-nowrap">Show per page:</span>
              <Select
                onValueChange={(value) => {
                  router.push(
                    `/ideas?page[number]=${currentPage}&page[size]=${value}&sort=${currentSort}`,
                    { scroll: false }
                  );
                }}
                defaultValue={currentPageSize.toString()}
              >
                <SelectTrigger className="rounded-full pl-4 w-28 border-gray-400">
                  <SelectValue />
                </SelectTrigger>
                <SelectContent>
                  <SelectGroup>
                    <SelectItem value="10">10</SelectItem>
                    <SelectItem value="20">20</SelectItem>
                    <SelectItem value="50">50</SelectItem>
                  </SelectGroup>
                </SelectContent>
              </Select>
            </div>
            <div className="flex gap-2 items-center">
              <span className="whitespace-nowrap">Sort By:</span>
              <Select
                onValueChange={(value) => {
                  router.push(
                    `/ideas?page[number]=${currentPage}&page[size]=${currentPageSize}&sort=${value}`,
                    { scroll: false }
                  );
                }}
                defaultValue={"-published_at"}
              >
                <SelectTrigger className="rounded-full pl-4 w-28 border-gray-400">
                  <SelectValue />
                </SelectTrigger>
                <SelectContent>
                  <SelectGroup>
                    <SelectItem value="-published_at">Newest</SelectItem>
                    <SelectItem value="published_at">Oldest</SelectItem>
                  </SelectGroup>
                </SelectContent>
              </Select>
            </div>
          </div>
        </div>

        <div className="grid gap-5 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          {isLoading && <CardSkeleton size={8} />}
          {!isLoading &&
            ideasList?.map(({ title, published_at, id, medium_image }) => {
              return (
                <Card
                  key={id}
                  published_at={published_at}
                  title={title}
                  image_url={medium_image[0]?.url}
                />
              );
            })}
        </div>

        {totalPage && (
          <div className="flex w-full mt-10 justify-center gap-2 items-center">
            <div className="flex">
              <button
                onClick={() =>
                  router.push(
                    `/ideas?page[number]=1&page[size]=${currentPageSize}&sort=${currentSort}`,
                    { scroll: false }
                  )
                }
                disabled={currentPage === 1}
                className="flex items-center disabled:text-gray-300"
              >
                <ChevronDoubleLeftIcon className="w-5 h-5" />
              </button>
              <button
                onClick={() =>
                  router.push(
                    `/ideas?page[number]=${
                      currentPage - 1
                    }&page[size]=${currentPageSize}&sort=${currentSort}`,
                    { scroll: false }
                  )
                }
                disabled={currentPage - 1 <= 0}
                className="flex items-center disabled:text-gray-300"
              >
                <ChevronLeftIcon className="w-5 h-5" />
              </button>
            </div>

            <div className="flex font-medium items-center">
              {paginationNavItems.map((num, index) => {
                return (
                  <Link
                    scroll={false}
                    href={`/ideas?page[number]=${num}&page[size]=${currentPageSize}&sort=${currentSort}`}
                    key={index}
                    className={`py-2 px-3 rounded-md hover:bg-gray-100 ${
                      currentPage === num && "bg-suitmedia-orange text-white"
                    }`}
                  >
                    {num}
                  </Link>
                );
              })}
            </div>

            <div className="flex">
              <button
                onClick={() =>
                  router.push(
                    `/ideas?page[number]=${
                      currentPage + 1
                    }&page[size]=${currentPageSize}&sort=${currentSort}`,
                    { scroll: false }
                  )
                }
                disabled={currentPage + 1 > totalPage}
                className="flex items-center disabled:text-gray-300"
              >
                <ChevronRightIcon className="w-5 h-5" />
              </button>
              <button
                onClick={() =>
                  router.push(
                    `/ideas?page[number]=${totalPage}&page[size]=${currentPageSize}&sort=${currentSort}`,
                    { scroll: false }
                  )
                }
                disabled={currentPage >= totalPage}
                className="flex items-center disabled:text-gray-300"
              >
                <ChevronDoubleRightIcon className="w-5 h-5" />
              </button>
            </div>
          </div>
        )}
      </section>
    </main>
  );
}

export default IdeasPage;
