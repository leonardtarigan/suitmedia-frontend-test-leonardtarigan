import { GetIdeasProps, IdeasResponse } from "@/lib/types";
import axios from "axios";

const ideas = async ({
  page,
  pageSize,
  append,
  sort,
}: GetIdeasProps): Promise<IdeasResponse | null> => {
  try {
    const response = await axios.get(
      "https://suitmedia-backend.suitdev.com/api/ideas",
      {
        params: {
          "page[number]": page,
          "page[size]": pageSize,
          append: append,
          sort: sort,
        },
      }
    );
    return response.data;
  } catch (error) {
    return null;
  }
};

const GET = {
  ideas,
};

export default GET;
