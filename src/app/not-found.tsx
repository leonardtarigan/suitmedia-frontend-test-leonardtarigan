export default function NotFoundPage() {
  return (
    <main className="h-screen flex justify-center items-center font-bold text-3xl">
      Not Found
    </main>
  );
}
